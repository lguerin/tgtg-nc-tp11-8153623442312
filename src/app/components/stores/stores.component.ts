import { Component, OnInit } from '@angular/core';
import { StoreModel } from '../../models/store.model';
import { Category } from '../../constants/category.constant';
import { Title } from '@angular/platform-browser';
import { TgtgService } from '../../services/tgtg.service';

@Component({
  selector: 'nc-stores',
  templateUrl: './stores.component.html',
  styleUrls: ['./stores.component.scss']
})
export class StoresComponent implements OnInit {

  withEmpty = true;
  stores: Array<StoreModel> = [];
  categories: Array<string> = [];

  constructor(private title: Title, private tgtgService: TgtgService) {
    this.title.setTitle(`TGTG NC - Page d'accueil`);
  }

  ngOnInit(): void {
    this.stores = this.tgtgService.getAllStores();
    this.categories = this.getAllCategories();
  }

  /**
   * Récupérer toutes les catégories possibles
   */
  private getAllCategories(): Array<string> {
    return Object.keys(Category).sort();
  }

  /**
   * Filtrer les paniers des commerces par catégorie
   * @param category  catégorie de panier
   */
  getStoresByCategory(category: string): Array<StoreModel> {
    return this.stores.filter(s => s.category === category);
  }

  /**
   * Filtrer les paniers vides ou non
   * @param value Filtre sur les paniers vides
   */
  filterEmpty(value: boolean): void {
    this.withEmpty = value;
  }
}
